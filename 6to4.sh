##########################################################################
# Title      : 6to4.sh
# Author     : Adrien Schildknecht <adrien+dev@schischi.me>
# Requires   : ip
# Category   : network
##########################################################################
# Description
#  Create a 6to4 tunnel.
##########################################################################

#!/usr/bin/env bash

device="eth0"
action=$1
ip4=$2

function usage() {
echo "Syntax: $0 <start|stop> <a.b.c.d> [device]"
exit 1
}

if [ "$action" = "start" ]
then
    echo "$ip4" | grep -q '^[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*$'
    if [ $? -ne "0" ]
    then
        usage
    fi
else
    if [ "$action" != "stop" ]
    then
        usage
    fi
fi

if [ $# -eq 3 ]
then
    device=$3
fi

if [ "$action" = "start" ]
then
    prefix=$(printf '%02x%02x:%02x%02x\n' $(echo $ip4 | sed 's/\./ /g'))
    ip tunnel add 6to4_sjtu mode sit remote any local $ip4
    ip link set dev 6to4_sjtu up
    ip addr add 2002:$prefix::1/16 dev 6to4_sjtu
    ip -6 route add ::/0 via ::192.88.99.1 dev 6to4_sjtu metric 1026
else
    ip -6 route flush dev 6to4_sjtu
    ip link set dev 6to4_sjtu down
    ip tunnel del 6to4_sjtu
fi

