##########################################################################
# Title      : ap.sh
# Author     : Adrien Schildknecht <adrien+dev@schischi.me>
# Requires   : hostapd, dnsmasq, iptables
# Category   : Network
##########################################################################
# Description
#  Create an access point protected with WPA sharing the internet
#  connection from wired interface.
##########################################################################

#!/usr/bin/env bash

internet="enp4s0"
wifi="wlo1"
channel="6"
driver="nl80211"
ssid="wifibot"
passphrase="12345678"
IP="192.168.42"
netmask="255.255.255.0"
conf="/tmp/hostapd.tmp"

case "$1" in
  start)
    echo interface=$wifi > $conf
    echo channel=$channel >> $conf
    echo driver=$driver >> $conf
    echo ssid=$ssid >> $conf
    echo wpa_passphrase=$passphrase >> $conf
    echo wpa=3 >> $conf
    echo wpa_key_mgmt=WPA-PSK >> $conf
    echo wpa_pairwise=TKIP CCMP >> $conf
    echo hw_mode=g >> $conf
    ifconfig $wifi down
    ifconfig $wifi $IP.1 netmask $netmask up
    hostapd -B $conf
    rm $conf
    dnsmasq --interface $wifi --cache-size=256 --dhcp-range=$IP.2,$IP.3,1h
    systemctl start iptables
    iptables -F
    iptables -X
    iptables -t nat -A POSTROUTING -o $internet -j MASQUERADE
    echo 1 > /proc/sys/net/ipv4/ip_forward
  ;;
  stop)
    kill -15 `pidof hostapd`
    kill -15 `pidof dnsmasq`
    ifconfig $wifi down
    systemctl stop iptables
    echo 0 > /proc/sys/net/ipv4/ip_forward
  ;;
  restart)
    $0 stop
    $0 start
  ;;
  status)
    echo "pidof hostapd: `pidof hostapd`"
    echo "pidof dnmasq: `pidof dnsmasq`"
  ;;
  *)
    echo "Usage: $0 {start|stop|restart|status}" >&2
    exit 1
  ;;
esac

exit 0
