##########################################################################
# Title      : isatao.sh
# Author     : Adrien Schildknecht <adrien+dev@schischi.me>
# Requires   : ip, isatapd
# Category   : network
##########################################################################
# Description
#  Create a isatap tunnel.
##########################################################################

#!/usr/bin/env bash

action=$1
ip4=$2

function usage() {
    echo "Syntax: $0 <start|stop> <a.b.c.d>"
    exit 1
}

if [ "$action" = "start" ]
then
    echo "$ip4" | grep -q '^[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*$'
    if [ $? -ne "0" ]
    then
        usage
    fi
else
    if [ "$action" != "stop" ]
    then
        usage
    fi
fi

if [ "$action" = "start" ]
then
    isatapd -d isatap.sjtu.edu.cn
    ip tunnel add is_sjtu mode isatap remote 202.112.26.246 local $ip4
    ip link set is_sjtu up
    ip tunnel prl prl-default 202.112.26.246 dev is_sjtu
    ip -6 route add default via fe80::5efe:202.112.26.246 dev is_sjtu
else
    ip -6 route flush dev is_sjtu
    ip link set dev is_sjtu down
    ip tunnel del is_sjtu
    ip link set dev is0 down
    ip tunnel del is0
fi
