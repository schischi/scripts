##########################################################################
# Title      : swap.sh
# Author     : Adrien Schildknecht <adrien+dev@schischi.me>
# Requires   : -
# Category   : sysutils
##########################################################################
# Description
#  Safely seap wap 2 files.
##########################################################################

#!/usr/bin/env bash

if [ $# -ne 2 ]
then
    echo Swap 2 files safely
    echo Usage: $0 file1 file2
    exit 1
fi
if [ ! -e $1]
then
    echo File $1 does not exist.
fi
if [ ! -e $2]
then
    echo File $2 does not exist.
fi

tmpfile=$(mktemp $(dirname "$1")/XXXXXX)
mv "$1" "$tmpfile" && mv "$2" "$1" && mv "$tmpfile" "$2"
