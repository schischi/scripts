##########################################################################
# Title      : time.sh
# Author     : Adrien Schildknecht <adrien+dev@schischi.me>
# Requires   :
# Category   : network
##########################################################################
# Description
#  Set the system date and time using google.fr.
##########################################################################

#!/bin/sh

date -s "$(wget -S  "http://www.google.fr/" 2>&1 | grep -E \
'^[[:space:]]*[dD]ate:' | sed 's/^[[:space:]]*[dD]ate:[[:space:]]*//' | head \
-1l | awk '{print $1, $3, $2,  $5 ,"GMT", $4 }' | sed 's/,//')"
